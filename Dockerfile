FROM registry.esss.lu.se/ics-docker/conda-build:latest

USER root
RUN yum install -y python3-pip iputils && \
  pip3 install docker-compose
USER conda
